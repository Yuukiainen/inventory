from rest_framework import serializers
from blogs.models.equipment import Equipment, RackEquipment, MachineType
from blogs.models.rack import Rack
from blogs.models.customer import Customer
from blogs.models.ip import IPNetworkk, IPAddresss



class EquipmentSerializers(serializers.ModelSerializer):
    class Meta:
        model = Equipment
        fields = (
                    'id',
                    'serial_number',
                    'machine_name',
                    'machine_type',
                    'machine_weight',
                    'electric_power',
                    'user',
                    'customer',
                    'image',
                    'code_type',
                    'code',
                    'auto_generate',
                 )
class RackEquipmentSerializers(serializers.ModelSerializer):
    class Meta:
        model = RackEquipment
        fields = (
                    'id',
                    'equipment',
                    'rack',
                    'front_back',
                    'unit_number',
                 )
class EquipmentTypeSerializers(serializers.ModelSerializer):
    class Meta:
        model = MachineType
        fields = (
                    'id',
                    'type_machine',
                 )
class RackSerializers(serializers.ModelSerializer):
    class Meta:
        model = Rack
        fields = (
                    'id',
                    'rack',
                    'acceptable_weight',
                    'rack_weight',
                    'legs_weight',
                 )

########################################################################

class IPNetworkSerializers(serializers.ModelSerializer):
    class Meta:
        model = IPNetworkk
        fields = (
                    'id',
                    'ip_network',
                    'subnet',
                    'customer',
                    'user',
                    'note',
                 )