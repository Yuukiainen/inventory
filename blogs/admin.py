from django.contrib import admin
from blogs.models.ip import IPNetworkk, IPAddresss
from blogs.models.customer import Customer
from blogs.models.equipment import MachineType, RackEquipment, Equipment 
from blogs.models.rack import Rack
from blogs.models.service import Service, ServiceType, ServiceEquipment, ServiceIPAddress
from blogs.models.user_data import UserData
from blogs.models.location import Location


admin.site.register(IPNetworkk)
admin.site.register(IPAddresss)
admin.site.register(Customer)
admin.site.register(MachineType)
admin.site.register(RackEquipment)
admin.site.register(Equipment)
admin.site.register(Rack)
admin.site.register(Service)
admin.site.register(ServiceType)
admin.site.register(ServiceEquipment)
admin.site.register(ServiceIPAddress)
admin.site.register(UserData)
admin.site.register(Location)
