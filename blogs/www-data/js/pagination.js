var table = '#myTable'
var lastpage = 0
var i = 1
$(document).ready(function() {
    var maxRows = 10
    dataTable(maxRows)
});

function dataTable(maxRows) {
    $('.pagination').html('')
    var trnum = 0
    var totalRows = $(table + ' tbody tr').length
    $(table + ' tr:gt(0)').each(function() {
        trnum++
        if (trnum > maxRows)
            $(this).hide()
        if (trnum <= maxRows)
            $(this).show()
    })
    if (totalRows > maxRows) {
        var pagenum = Math.ceil(totalRows / maxRows)
        $('.pagination').append('<li class="page-item" data-page="1"  value="0" id="first"><span class="page-link">First<span class="sr-only">(current)</span></span></li>').show()
        while (i <= pagenum) {
            $('.pagination').append('<li class="page-item" data-page="' + i + '" value="' + i + '" id="' + i + '"><span  class="page-link">' + i++ + '<span class="sr-only">(current)</span></span></li>').show()
        }
        i = i - 1
        $('.pagination').append('<li class="page-item" data-page="' + i + '" value="-1" id="last"><span  class="page-link">Last<span class="sr-only">(current)</span></span></li>').show()
    }
    $('.pagination li:first-child').addClass('active')
    $('.pagination li').on('click', function() {
        var pageNum = $(this).attr('data-page')
        var trIndex = 0
        $('.pagination li').removeClass('active')
        $(this).addClass('active')
        page = parseInt($(this).val())
        $(table + ' tr:gt(0)').each(function() {
            trIndex++
            if (trIndex > (maxRows * pageNum) || trIndex <= ((maxRows * pageNum) - maxRows))
                $(this).hide()
            else
                $(this).show()
        })
    })

}