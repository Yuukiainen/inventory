from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views import generic
from blogs.forms.register import RegistrationForm
from blogs.models.user_data import UserData
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect, JsonResponse, HttpResponse
from django.contrib.auth.views import (
    PasswordChangeView,
    PasswordChangeDoneView,
)


class Register(generic.CreateView):
    model = User
    form_class = RegistrationForm
    template_name = 'registration/register.html'
    def get_success_url(self): 
        return reverse_lazy('profile', kwargs={'pk': self.request.user.id})
    # ส่งข้อมูลไปหน้า html 
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # ป้องกัน error user ที่ไม่ได้ login
        try:
            context['user_data'] = UserData.objects.get(user=self.request.user.id)
        except:
            context['user'] = UserData.objects.last()
        return context
    # หนดสิ่งที่จะเกิดขึ้นหลังกดปุ่ม submit และหลังจากตรวจสอบข้อมูลในหน้าฟอร์มเสร็จแล้ว
    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.save()
        user = User.objects.last()
        # กำหนดสิทธ์ user ให้เป็น admin 
        if self.request.POST.get('user_status') == 'admin':
            user.is_superuser = True
            user.is_staff = True
            user.save()
        # save ข้อมูลเพิ่มเติมเพื่อเรียกใช้
        user = UserData(
                         first_name = str((User.objects.last()).first_name),
                         last_name = str((User.objects.last()).last_name),
                         email = str((User.objects.last()).email),
                         user=User.objects.last(),
                       )
        user.save()
        return HttpResponseRedirect(self.get_success_url()) 
