from django.shortcuts import render, redirect
from django.views.generic import CreateView, UpdateView, ListView, TemplateView
from blogs.models.ip import IPNetworkk
from blogs.models.equipment import Equipment, RackEquipment
from blogs.models.user_data import UserData
from blogs.models.service import Service
from blogs.models.rack import Rack
from django.contrib.auth.models import User
from blogs.forms.register import UpdateUserDataForm, UpdateImageForm
from django.urls import reverse_lazy
from django.http import HttpResponseRedirect, JsonResponse

class ProfileView(UpdateView):
    model = User
    form_class = UpdateImageForm
    context_object_name = 'user_auth'
    template_name = 'profile/profile-view.html'
    # เมื่อกดเพิ่มข้อมูลและต้องการให้ไปยังหน้า detail ที่ต้องใช้ id ในการเข้าถึง
    def get_success_url(self): 
        return reverse_lazy('profile', kwargs={'pk': self.kwargs['pk']})
    # ส่งข้อมูลไปหน้า html 
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # ป้องกัน error user ที่ไม่ได้ login
        try:
            context['user_data'] = UserData.objects.get(user=self.kwargs['pk'])
        except:
            context['user'] = UserData.objects.last()
        return context
    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['user_data_id'] = (UserData.objects.get(user=self.kwargs['pk'])).id
        return kwargs

class UpdateProfile(UpdateView):
    model = UserData
    form_class = UpdateUserDataForm
    template_name = 'profile/update-profile.html'
    # ส่งข้อมูลไปหน้า html 
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # ป้องกัน error user ที่ไม่ได้ login
        try:
            context['user_data'] = UserData.objects.get(id=self.kwargs['pk'])
        except:
            context['user'] = UserData.objects.last()
        return context
    # เมื่อกดเพิ่มข้อมูลและต้องการให้ไปยังหน้า detail ที่ต้องใช้ id ในการเข้าถึง
    def get_success_url(self): 
        return reverse_lazy('profile', kwargs={'pk': (UserData.objects.get(id=self.kwargs['pk'])).user.id})
    # หนดสิ่งที่จะเกิดขึ้นหลังกดปุ่ม submit และหลังจากตรวจสอบข้อมูลในหน้าฟอร์มเสร็จแล้ว
    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.save()
        user = User.objects.get(id=self.request.user.id)
        user.first_name = str((UserData.objects.get(user=self.request.user.id)).first_name)
        user.last_name = str((UserData.objects.get(user=self.request.user.id)).last_name)
        user.email = str((UserData.objects.get(user=self.request.user.id)).email)
        user.save()
        return HttpResponseRedirect(self.get_success_url())