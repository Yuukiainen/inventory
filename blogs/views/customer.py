from django.shortcuts import render, redirect
from django.views.generic import CreateView, UpdateView, ListView, DetailView, DeleteView
from blogs.models.customer import Customer
from blogs.forms.customer import AddCustomerForm, UpdateCustomerForm
from django.urls import reverse_lazy
from django.http import HttpResponseRedirect, JsonResponse, HttpResponse
from blogs.models.user_data import UserData
from blogs.models.equipment import RackEquipment
from blogs.models.ip import IPAddresss
from blogs.models.service import Service


class AddCustomer(CreateView):
    model = Customer
    form_class = AddCustomerForm
    template_name = 'customer/add-customer.html'
    # ส่งข้อมูลไปหน้า html 
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # ป้องกัน error user ที่ไม่ได้ login
        try:
            context['user_data'] = UserData.objects.get(user=self.request.user.id)
        except:
            context['user'] = UserData.objects.last()
        return context
    # หนดสิ่งที่จะเกิดขึ้นหลังกดปุ่ม submit และหลังจากตรวจสอบข้อมูลในหน้าฟอร์มเสร็จแล้ว
    def form_valid(self, form):
        form.save(True)  
        return HttpResponse('<script type="text/javascript">window.close()</script>')

        
class AddCustomerTemplateView(CreateView):
    model = Customer
    form_class = AddCustomerForm
    success_url = reverse_lazy('customer-listview')
    template_name = 'customer/add-customer-template-view.html'
    # ส่งข้อมูลไปหน้า html 
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # ป้องกัน error user ที่ไม่ได้ login
        try:
            context['user_data'] = UserData.objects.get(user=self.request.user.id)
        except:
            context['user'] = UserData.objects.last()
        return context

class UpdateCustomerTemplateView(UpdateView):
    model = Customer
    form_class = UpdateCustomerForm
    template_name = 'customer/update-customer.html'
    # เมื่อกดเพิ่มข้อมูลและต้องการให้ไปยังหน้า detail ที่ต้องใช้ id ในการเข้าถึง
    def get_success_url(self): 
        return reverse_lazy('customer-detail', kwargs={'pk': self.kwargs['pk']})
    # ส่งข้อมูลไปหน้า html 
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # ป้องกัน error user ที่ไม่ได้ login
        try:
            context['user_data'] = UserData.objects.get(user=self.request.user.id)
        except:
            context['user'] = UserData.objects.last()
        return context

class CustomerView(ListView):
    model = Customer
    context_object_name = 'customer'
    template_name = 'customer/customer-listview.html'
    # ส่งข้อมูลไปหน้า html 
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # ป้องกัน error user ที่ไม่ได้ login
        try:
            context['user_data'] = UserData.objects.get(user=self.request.user.id)
        except:
            context['user'] = UserData.objects.last()
        return context

class deleteCustomer(DeleteView):
    model = Customer
    success_url = reverse_lazy('customer-listview')
    def get(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)
        
class CustomerDetail(DetailView):
    model = Customer
    context_object_name = 'customer'
    template_name = 'customer/customer-detail.html'
    # ส่งข้อมูลไปหน้า html 
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['rackequipment'] = RackEquipment.objects.filter(equipment__customer=self.kwargs['pk'])
        context['ip_address'] = IPAddresss.objects.filter(customer=self.kwargs['pk'])
        context['service'] = Service.objects.filter(customer=self.kwargs['pk'])
        # ป้องกัน error user ที่ไม่ได้ login
        try:
            context['user_data'] = UserData.objects.get(user=self.request.user.id)
        except:
            context['user'] = UserData.objects.last()
        return context