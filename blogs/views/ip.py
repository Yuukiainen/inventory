from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import CreateView, UpdateView, ListView, DetailView, DeleteView
from blogs.models.ip import IPNetworkk, IPAddresss
from blogs.models.customer import Customer
from blogs.forms.ip import AddIPNetworkForm, AddIPAddressForm, DeleteIPAddressForm, UpdateIPNetworkForm
from blogs.models.service import ServiceIPAddress
from django.urls import reverse_lazy
from django.http import HttpResponseRedirect, JsonResponse
from ipaddress import ip_network
from netaddr import IPNetwork
from blogs.models.user_data import UserData
from django.http import HttpResponseRedirect, JsonResponse, HttpResponse


class IPNetworkView(ListView):
    model = IPNetworkk
    context_object_name = 'ip_network'
    template_name = 'ip/ip-listview.html'
    # ส่งข้อมูลไปหน้า html 
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        hosts = []
        hosts_create = []
        for x in IPNetworkk.objects.all():
            if(((IPNetwork(str(x.ip_network) + '/' + str(x.subnet))).size)-2) < 0:
                hosts.append(0)
            else:
                hosts.append(((IPNetwork(str(x.ip_network) + '/' + str(x.subnet))).size)-2)
            hosts_create.append(len(IPAddresss.objects.filter(ip_network=x.id)))
        context['hosts'] = hosts
        context['hosts_create'] = hosts_create
        context['ip_address_create'] = IPAddresss.objects.all()
        context['user_data_all'] = UserData.objects.all()
        # ป้องกัน error user ที่ไม่ได้ login
        try:
            context['user_data'] = UserData.objects.get(user=self.request.user.id)
        except:
            context['user'] = UserData.objects.last()
        return context


class AddIPNetwork(CreateView):
    model = IPNetworkk
    form_class = AddIPNetworkForm
    success_url = reverse_lazy('ip-listview')
    template_name = 'ip/add-network.html'
    # ส่งข้อมูลไปหน้า html 
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # ป้องกัน error user ที่ไม่ได้ login
        try:
            context['user_data'] = UserData.objects.get(user=self.request.user.id)
        except:
            context['user'] = UserData.objects.last()
        return context
    # หนดสิ่งที่จะเกิดขึ้นหลังกดปุ่ม submit และหลังจากตรวจสอบข้อมูลในหน้าฟอร์มเสร็จแล้ว
    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.user = self.request.user
        self.object.save()
        return HttpResponseRedirect(self.get_success_url())  

class UpdateIPNetwork(UpdateView):
    model = IPNetworkk
    form_class = UpdateIPNetworkForm
    success_url = reverse_lazy('ip-listview')
    template_name = 'ip/update-ip-network.html'
    # ส่งข้อมูลไปหน้า html 
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # ป้องกัน error user ที่ไม่ได้ login
        try:
            context['user_data'] = UserData.objects.get(user=self.request.user.id)
        except:
            context['user'] = UserData.objects.last()
        return context
    # หนดสิ่งที่จะเกิดขึ้นหลังกดปุ่ม submit และหลังจากตรวจสอบข้อมูลในหน้าฟอร์มเสร็จแล้ว
    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.user = self.request.user
        self.object.save()
        return HttpResponseRedirect(self.get_success_url())  


class AddIPAddress(CreateView):
    model = IPAddresss
    form_class = AddIPAddressForm
    template_name = 'ip/add-address.html'
    # เมื่อกดเพิ่มข้อมูลและต้องการให้ไปยังหน้า detail ที่ต้องใช้ id ในการเข้าถึง
    def get_success_url(self): 
        ip_address = IPAddresss.objects.last()
        return reverse_lazy('ip-detail', kwargs={'pk': ip_address.ip_network.id})
    # ส่งข้อมูลไปหน้า html 
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['ipnetwork'] = IPNetworkk.objects.get(pk=self.kwargs['pk'])
        # ป้องกัน error user ที่ไม่ได้ login
        try:
            context['user_data'] = UserData.objects.get(user=self.request.user.id)
        except:
            context['user'] = UserData.objects.last()
        return context


class IPNetworkDetail(UpdateView):
    model = IPNetworkk 
    context_object_name = 'network'
    form_class = DeleteIPAddressForm
    template_name = 'ip/ip-detail.html'
    # เมื่อกดเพิ่มข้อมูลและต้องการให้ไปยังหน้า detail ที่ต้องใช้ id ในการเข้าถึง
    def get_success_url(self): 
        return reverse_lazy('ip-detail', kwargs={'pk': self.kwargs['pk']})
    # ส่งข้อมูลไปหน้า form
    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['ipnetwork'] = IPNetworkk.objects.get(id=self.kwargs['pk'])
        return kwargs
    # ส่งข้อมูลไปหน้า html 
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['id'] = self.kwargs['pk']
        network = IPNetworkk.objects.get(pk=self.kwargs['pk'])
        context['ip_address_list'] = list(ip_network(str(network.ip_network)+'/'+str(network.subnet)).hosts())
        context['ip_address'] = IPAddresss.objects.filter(ip_network=self.kwargs['pk'])
        context['lenght_ip_address'] = len(IPAddresss.objects.filter(ip_network=self.kwargs['pk']))
        context['customer'] = Customer.objects.all()
        context['user_data_all'] = UserData.objects.all()
        # ป้องกัน error user ที่ไม่ได้ login
        try:
            context['user_data'] = UserData.objects.get(user=self.request.user.id)
        except:
            context['user'] = UserData.objects.last()
        return context

class DeleteIPAddress(DeleteView):
    model = IPAddresss
    # เมื่อกดเพิ่มข้อมูลและต้องการให้ไปยังหน้า detail ที่ต้องใช้ id ในการเข้าถึง
    def get_success_url(self): 
        ip_address = IPAddresss.objects.get(id=self.kwargs['pk'])
        return reverse_lazy('ip-detail', kwargs={'pk': ip_address.ip_network.id})
    def get(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)

class DeleteIPNetwork(DeleteView):
    model = IPNetworkk
    success_url = reverse_lazy('ip-listview')
    def get(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)