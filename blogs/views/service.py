from django.shortcuts import render
from django.views.generic import CreateView, UpdateView, ListView, DeleteView, DetailView
from blogs.models.service import Service, ServiceType, ServiceEquipment, ServiceIPAddress
from blogs.models.equipment import Equipment
from blogs.models.ip import IPNetworkk, IPAddresss
from blogs.models.customer import Customer
from django.contrib.auth.models import User
from blogs.forms.service import AddServiceForm, AddServiceEquipmentForm, AddServiceIPAddressForm, AddServiceTypeForm
from django.urls import reverse_lazy
from django.http import HttpResponseRedirect, JsonResponse, HttpResponse
from blogs.models.user_data import UserData
from blogs.models.customer import Customer
from django.db.models import Q

class ServiceView(ListView):
    model = Service
    context_object_name = 'service'
    template_name = 'service/service-view.html'
    # ส่งข้อมูลไปหน้า html 
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['serviceequipment'] = ServiceEquipment.objects.all()
        context['serviceipaddress'] = ServiceIPAddress.objects.all()
        context['user_data_all'] = UserData.objects.all()
        context['customer'] = Customer.objects.all()
        # ป้องกัน error user ที่ไม่ได้ login
        try:
            context['user_data'] = UserData.objects.get(user=self.request.user.id)
        except:
            context['user'] = UserData.objects.last()
        return context

class ServiceDetail(DetailView):
    model = Service
    context_object_name = 'service'
    template_name = 'service/service-detail.html'
    # ส่งข้อมูลไปหน้า html 
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        serviceequipment = ServiceEquipment.objects.filter(service=self.kwargs['pk'])
        context['serviceequipment'] = serviceequipment
        context['user_data_all'] = UserData.objects.all()
        context['customer'] = Customer.objects.all()
        context['serviceipaddress'] = ServiceIPAddress.objects.filter(service=self.kwargs['pk'])
        # ป้องกัน error user ที่ไม่ได้ login
        try:
            context['user_data'] = UserData.objects.get(user=self.request.user.id)
        except:
            context['user'] = UserData.objects.last()
        return context

class AddService(CreateView):
    model = Service
    form_class = AddServiceForm
    success_url = reverse_lazy('service-listview')
    template_name = 'service/add-service.html'
    # ส่งข้อมูลไปหน้า html 
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # ป้องกัน error user ที่ไม่ได้ login
        try:
            context['user_data'] = UserData.objects.get(user=self.request.user.id)
        except:
            context['user'] = UserData.objects.last()
        return context
    # หนดสิ่งที่จะเกิดขึ้นหลังกดปุ่ม submit และหลังจากตรวจสอบข้อมูลในหน้าฟอร์มเสร็จแล้ว
    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.user = self.request.user
        self.object.save()
        return HttpResponseRedirect(self.get_success_url())  


class DeleteService(DeleteView):
    model = Service
    success_url = reverse_lazy('service-listview')
    def get(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)

class DeleteServiceIPAddress(DeleteView):
    model = ServiceIPAddress
    # เมื่อกดเพิ่มข้อมูลและต้องการให้ไปยังหน้า detail ที่ต้องใช้ id ในการเข้าถึง
    def get_success_url(self): 
        service_ip_address = ServiceIPAddress.objects.get(id=self.kwargs['pk'])
        return reverse_lazy('report-service', kwargs={'pk': service_ip_address.service.id})
    def get(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)

class DeleteServiceEquipment(DeleteView):
    model = ServiceEquipment
    # เมื่อกดเพิ่มข้อมูลและต้องการให้ไปยังหน้า detail ที่ต้องใช้ id ในการเข้าถึง
    def get_success_url(self):
        service_equipment = ServiceEquipment.objects.get(id=self.kwargs['pk'])
        (ServiceIPAddress.objects.filter(Q(service=service_equipment.service.id) and Q(ip_address__equipment=service_equipment.equipment.id))).delete()
        return reverse_lazy('report-service', kwargs={'pk': service_equipment.service.id})
    def get(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)


class AddServiceEquipment(CreateView):
    model = ServiceEquipment
    form_class = AddServiceEquipmentForm
    template_name = 'service/add-serviceequipment.html'
    # ส่งข้อมูลไปหน้า html 
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        _id = self.kwargs['pk']
        context['id'] = str(_id)
        context['equipment'] = Equipment.objects.filter(user=self.request.user)
        context['service'] = Service.objects.all()
        # ป้องกัน error user ที่ไม่ได้ login
        try:
            context['user_data'] = UserData.objects.get(user=self.request.user.id)
        except:
            context['user'] = UserData.objects.last()
        return context
    # หนดสิ่งที่จะเกิดขึ้นหลังกดปุ่ม submit และหลังจากตรวจสอบข้อมูลในหน้าฟอร์มเสร็จแล้ว
    def form_valid(self, form):
        form.save(True)  
        return HttpResponse('<script type="text/javascript">window.close()</script>')

class AddServiceIPAddress(CreateView):
    model = ServiceEquipment
    form_class = AddServiceIPAddressForm
    template_name = 'service/add-serviceipaddress.html'
    # ส่งข้อมูลไปหน้า html 
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        _id = self.kwargs['pk']
        context['id'] = str(_id)
        context['service'] = Service.objects.all()
        context['ip_address'] = IPAddresss.objects.all()
        context['equipment'] = Equipment.objects.filter(user=self.request.user)
        # ป้องกัน error user ที่ไม่ได้ login
        try:
            context['user_data'] = UserData.objects.get(user=self.request.user.id)
        except:
            context['user'] = UserData.objects.last()
        return context
    # ส่งข้อมูลไปหน้า form
    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['user'] = self.request.user
        return kwargs
    # หนดสิ่งที่จะเกิดขึ้นหลังกดปุ่ม submit และหลังจากตรวจสอบข้อมูลในหน้าฟอร์มเสร็จแล้ว
    def form_valid(self, form):
        form.save(True)  
        return HttpResponse('<script type="text/javascript">window.close()</script>')

class AddServiceType(CreateView):
    model = ServiceType
    form_class = AddServiceTypeForm
    template_name = 'service/add-type-service.html'
    # หนดสิ่งที่จะเกิดขึ้นหลังกดปุ่ม submit และหลังจากตรวจสอบข้อมูลในหน้าฟอร์มเสร็จแล้ว
    def form_valid(self, form):
        form.save(True)  
        return HttpResponse('<script type="text/javascript">window.close()</script>')