from django.views.generic import CreateView, UpdateView, ListView, DetailView, DeleteView
from blogs.forms.location import AddLocationForm, UpdateLocationForm
from django.urls import reverse_lazy
from blogs.models.user_data import UserData
from django.http import HttpResponseRedirect, JsonResponse, HttpResponse
from blogs.models.location import Location
from blogs.models.equipment import RackEquipment
from blogs.models.rack import Rack

class AddLocationTamplateView(CreateView):
    model = Location
    form_class = AddLocationForm
    template_name = 'location/add-location-tamplateview.html'
    success_url = reverse_lazy('location-listview')
    # ส่งข้อมูลไปหน้า html 
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # ป้องกัน error user ที่ไม่ได้ login
        try:
            context['user_data'] = UserData.objects.get(user=self.request.user.id)
        except:
            context['user'] = UserData.objects.last()
        return context

class UpdateLocation(UpdateView):
    model = Location
    form_class = UpdateLocationForm
    template_name = 'location/update-location.html'
    success_url = reverse_lazy('location-listview')
    # ส่งข้อมูลไปหน้า form
    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['id'] = self.kwargs['pk']
        return kwargs
    # ส่งข้อมูลไปหน้า html 
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # ป้องกัน error user ที่ไม่ได้ login
        try:
            context['user_data'] = UserData.objects.get(user=self.request.user.id)
        except:
            context['user'] = UserData.objects.last()
        return context



class LocationView(ListView):
    model = Location
    context_object_name = 'location'
    template_name = 'location/location-listview.html'
    # ส่งข้อมูลไปหน้า html 
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['rack_equipment'] = RackEquipment.objects.all()
        context['rack'] = Rack.objects.all()
        # ป้องกัน error user ที่ไม่ได้ login
        try:
            context['user_data'] = UserData.objects.get(user=self.request.user.id)
        except:
            context['user'] = UserData.objects.last()
        return context

class deleteLocation(DeleteView):
    model = Location
    success_url = reverse_lazy('location-listview')
    def get(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)