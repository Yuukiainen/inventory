from rest_framework import viewsets
from blogs.serializers.equipment import EquipmentSerializers, RackEquipmentSerializers, EquipmentTypeSerializers, RackSerializers
from blogs.serializers.ip import IPNetworkSerializers, IPAddressSerializers
from blogs.serializers.service import ServiceTypeSerializers, ServiceSerializers, ServiceEquipmentSerializers, ServiceIPAddressSerializers
from blogs.serializers.customer import CustomerSerializers
from blogs.models.equipment import Equipment, RackEquipment, MachineType
from blogs.models.rack import Rack
from blogs.models.customer import Customer
from blogs.models.ip import IPNetworkk, IPAddresss
from blogs.models.service import ServiceIPAddress, ServiceEquipment, ServiceType, Service
from django.core.exceptions import SuspiciousOperation
from rest_framework import permissions


class EquipmentSerializersView(viewsets.ModelViewSet):
    queryset = Equipment.objects.all()
    serializer_class = EquipmentSerializers

class RackEquipmentSerializersView(viewsets.ModelViewSet):
    queryset = RackEquipment.objects.all()
    serializer_class = RackEquipmentSerializers

class MachineTypeSerializersView(viewsets.ModelViewSet):
    queryset = MachineType.objects.all()
    serializer_class = EquipmentTypeSerializers
    permission_classes = (permissions.IsAdminUser, )

class RackSerializersView(viewsets.ModelViewSet):
    queryset = Rack.objects.all()
    serializer_class = RackSerializers

###########################################################################

class IPNetworkSerializersView(viewsets.ModelViewSet):
    queryset = IPNetworkk.objects.all()
    serializer_class = IPNetworkSerializers

class IPAddressSerializersView(viewsets.ModelViewSet):
    queryset = IPAddresss.objects.all()
    serializer_class = IPAddressSerializers

###########################################################################

class ServiceTypeSerializersView(viewsets.ModelViewSet):
    queryset = ServiceType.objects.all()
    serializer_class = ServiceTypeSerializers
    permission_classes = (permissions.IsAdminUser, )

class ServiceSerializersView(viewsets.ModelViewSet):
    queryset = Service.objects.all()
    serializer_class = ServiceSerializers

class ServiceEquipmentSerializersView(viewsets.ModelViewSet):
    queryset = ServiceEquipment.objects.all()
    serializer_class = ServiceEquipmentSerializers

class ServiceIPAddressSerializersView(viewsets.ModelViewSet):
    queryset = ServiceIPAddress.objects.all()
    serializer_class = ServiceIPAddressSerializers

###########################################################################

class CustomerSerializersView(viewsets.ModelViewSet):
    queryset = Customer.objects.all()
    serializer_class = CustomerSerializers