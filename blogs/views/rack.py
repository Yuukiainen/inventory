from django.views.generic import CreateView, UpdateView, ListView, DetailView, DeleteView
from blogs.models.rack import Rack
from blogs.models.equipment import RackEquipment, Equipment
from blogs.forms.rack import AddRackForm, UpdateRackForm
from django.urls import reverse_lazy
from blogs.models.user_data import UserData
from django.http import HttpResponseRedirect, JsonResponse, HttpResponse
from blogs.models.location import Location
from django.db.models import Q

class RackView(DetailView):
    model = Location
    context_object_name = 'location'
    template_name = 'rack/rack-listview.html'
    # ส่งข้อมูลไปหน้า html 
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['rack'] = Rack.objects.filter(location=self.kwargs['pk'])
        context['rackequipment'] = RackEquipment.objects.filter(rack__location=self.kwargs['pk'])
        context['location_all'] = Location.objects.all()
        # ป้องกัน error user ที่ไม่ได้ login
        try:
            context['user_data'] = UserData.objects.get(user=self.request.user.id)
        except:
            context['user'] = UserData.objects.last()
        return context

class ReportRack(DetailView):
    model = Rack
    context_object_name = 'rack'
    template_name = 'rack/report-rack.html'
    # ส่งข้อมูลไปหน้า html 
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        rack = Rack.objects.get(pk=self.kwargs['pk'])
        context['location'] = Location.objects.get(pk=int(rack.location.id))
        context['rack_inlocation'] = Rack.objects.filter(location=int(rack.location.id))
        context['rackequipment'] = RackEquipment.objects.filter(rack=self.kwargs['pk'])
        # ป้องกัน error user ที่ไม่ได้ login
        try:
            context['user_data'] = UserData.objects.get(user=self.request.user.id)
        except:
            context['user'] = UserData.objects.last()
        return context

class AddRack(CreateView):
    model = Rack
    form_class = AddRackForm
    template_name = 'rack/add-rack.html'
    # เมื่อกดเพิ่มข้อมูลและต้องการให้ไปยังหน้า detail ที่ต้องใช้ id ในการเข้าถึง
    def get_success_url(self): 
        rack = Rack.objects.last()
        return reverse_lazy('rack-listview', kwargs={'pk': rack.location.id})
    # ส่งข้อมูลไปหน้า html 
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['location_all'] = Location.objects.all()
        context['location'] = Location.objects.get(pk=self.kwargs['pk'])
        # ป้องกัน error user ที่ไม่ได้ login
        try:
            context['user_data'] = UserData.objects.get(user=self.request.user.id)
        except:
            context['user'] = UserData.objects.last()
        return context

class updateRack(UpdateView):
    model = Rack
    form_class = UpdateRackForm
    template_name = 'rack/update-rack.html'
    # ส่งข้อมูลไปหน้า form
    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['id'] = self.kwargs['pk']
        return kwargs
    # เมื่อกดเพิ่มข้อมูลและต้องการให้ไปยังหน้า detail ที่ต้องใช้ id ในการเข้าถึง
    def get_success_url(self): 
        rack = Rack.objects.get(pk=self.kwargs['pk'])
        return reverse_lazy('rack-listview', kwargs={'pk': rack.location.id})
    # ส่งข้อมูลไปหน้า html 
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        rack = Rack.objects.get(pk=self.kwargs['pk'])
        context['location_all'] = Location.objects.all()
        context['rack'] = rack
        context['location'] = Location.objects.get(pk=rack.location.id)
        # ป้องกัน error user ที่ไม่ได้ login
        try:
            context['user_data'] = UserData.objects.get(user=self.request.user.id)
        except:
            context['user'] = UserData.objects.last()
        return context

class DeleteRack(DeleteView):
    model = Rack
    # เมื่อกดเพิ่มข้อมูลและต้องการให้ไปยังหน้า detail ที่ต้องใช้ id ในการเข้าถึง
    def get_success_url(self): 
        rack = Rack.objects.get(pk=self.kwargs['pk'])
        return reverse_lazy('rack-listview', kwargs={'pk': rack.location.id})
    def get(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)
