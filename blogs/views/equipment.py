from django.shortcuts import render, get_object_or_404
from django.views.generic import CreateView, UpdateView, ListView, DeleteView, DetailView
from blogs.models.equipment import Equipment, RackEquipment, MachineType
from blogs.models.ip import IPAddresss, IPNetworkk
from blogs.models.customer import Customer
from blogs.models.rack import Rack
from blogs.forms.equipment import AddEquipmentForm, UpdateEquipmentForm, AddEquipmentTypeForm, MoveRachForm
from django.urls import reverse_lazy
from django.http import HttpResponseRedirect, JsonResponse
from django.utils import timezone
from django.db.models import Q
from django.http import HttpResponseRedirect, JsonResponse, HttpResponse
from blogs.models.user_data import UserData
from blogs.models.location import Location


class EquipmentView(ListView):
    model = RackEquipment
    context_object_name = 'rackequipment'
    template_name = 'equipment/equipment-view.html'
    # ส่งข้อมูลไปหน้า html 
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['customer'] = Customer.objects.all()
        context['rackequipment2'] = RackEquipment.objects.all()
        context['location'] = Location.objects.all()
        context['rack'] = Rack.objects.all()
        context['user_data_all'] = UserData.objects.all()
        # ป้องกัน error user ที่ไม่ได้ login
        try:
            context['user_data'] = UserData.objects.get(user=self.request.user.id)
        except:
            context['user'] = UserData.objects.last()
        return context

class EquipmentDetail(DetailView):
    model = Equipment
    context_object_name = 'equipment'
    template_name = 'equipment/equipment-detail.html'
    # ส่งข้อมูลไปหน้า html 
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['rack_equipment'] = RackEquipment.objects.filter(equipment=self.kwargs['pk'])
        context['rackequipment'] = (RackEquipment.objects.filter(equipment=self.kwargs['pk'])).last()
        context['customer'] = Customer.objects.all()
        context['ipaddress'] = len(IPAddresss.objects.filter(equipment=self.kwargs['pk']))
        context['user_data_id'] = (UserData.objects.get(user=(Equipment.objects.get(id=self.kwargs['pk'])).user.id)).id
        context['user_data_all'] = UserData.objects.all()
        unit = ''
        for x in RackEquipment.objects.filter(equipment=self.kwargs['pk']):
            if x == (RackEquipment.objects.filter(equipment=self.kwargs['pk'])).last():
                unit = unit + x.unit_number
            else:
                unit = unit + x.unit_number + ', '
        context['unit'] = unit
        # ป้องกัน error user ที่ไม่ได้ login
        try:
            context['user_data'] = UserData.objects.get(user=self.request.user.id)
        except:
            context['user'] = UserData.objects.last()
        return context

class AddEquipment(CreateView):
    model = Equipment
    form_class = AddEquipmentForm 
    # เมื่อกดเพิ่มข้อมูลและต้องการให้ไปยังหน้า detail ที่ต้องใช้ id ในการเข้าถึง
    def get_success_url(self): 
        rackequipment = RackEquipment.objects.last()
        return reverse_lazy('equipment-detail', kwargs={'pk': rackequipment.equipment.id})
    template_name = 'equipment/add-equipment.html'
    # ส่งข้อมูลไปหน้า form
    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['user'] = self.request.user
        return kwargs
    # ส่งข้อมูลไปหน้า html 
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['ip_network'] = IPNetworkk.objects.all()
        context['rack_equipment'] = RackEquipment.objects.all()
        context['rack'] = Rack.objects.all()
        context['equipment'] = Equipment.objects.last()
        context['equipment_all'] = Equipment.objects.all()
        context['customer'] = Customer.objects.all()
        context['location'] = Location.objects.all()
        # ป้องกัน error user ที่ไม่ได้ login
        try:
            context['user_data'] = UserData.objects.get(user=self.request.user.id)
        except:
            context['user'] = UserData.objects.last()
        return context

class AddEquipmentType(CreateView):
    model = MachineType
    form_class = AddEquipmentTypeForm
    template_name = 'equipment/add-type-equipment.html'
    # หนดสิ่งที่จะเกิดขึ้นหลังกดปุ่ม submit และหลังจากตรวจสอบข้อมูลในหน้าฟอร์มเสร็จแล้ว
    def form_valid(self, form):
        form.save(True)  
        return HttpResponse('<script type="text/javascript">window.close()</script>')

class MoveRack(UpdateView):
    model = Equipment
    context_object_name = 'equipment'
    form_class = MoveRachForm
    template_name = 'equipment/update-rackequipment.html'
    # เมื่อกดเพิ่มข้อมูลและต้องการให้ไปยังหน้า detail ที่ต้องใช้ id ในการเข้าถึง
    def get_success_url(self): 
        rackequipment = RackEquipment.objects.last()
        return reverse_lazy('equipment-detail', kwargs={'pk': rackequipment.equipment.id})
    # ส่งข้อมูลไปหน้า html 
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['rack_equipment'] = RackEquipment.objects.all()
        rack_equipment = (RackEquipment.objects.filter(equipment=self.kwargs['pk'])).last()
        context['rack_equipment_get'] = rack_equipment
        context['rack'] = Rack.objects.all()
        context['equipment_all'] = Equipment.objects.all()
        context['customer'] = Customer.objects.all()
        context['location'] = Location.objects.all()
        if Location.objects.filter(rack=int(rack_equipment.rack.id)):
            context['get_location'] = Location.objects.get(rack=int(rack_equipment.rack.id))
        # ป้องกัน error user ที่ไม่ได้ login
        try:
            context['user_data'] = UserData.objects.get(user=self.request.user.id)
        except:
            context['user'] = UserData.objects.last()
        return context

class updateEquipment(UpdateView):
    model = Equipment
    context_object_name = 'equipment'
    form_class = UpdateEquipmentForm
    template_name = 'equipment/update-equipment.html'
    # เมื่อกดเพิ่มข้อมูลและต้องการให้ไปยังหน้า detail ที่ต้องใช้ id ในการเข้าถึง
    def get_success_url(self): 
        return reverse_lazy('equipment-detail', kwargs={'pk': self.kwargs['pk']})
    # ส่งข้อมูลไปหน้า form
    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['id'] = self.kwargs['pk']
        return kwargs
    # ส่งข้อมูลไปหน้า html 
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        rack_equipment = RackEquipment.objects.filter(equipment=self.kwargs['pk'])
        rack_id = (rack_equipment.last()).rack.id
        context['rack'] = Rack.objects.get(id=int(rack_id))
        context['rackequipment'] = RackEquipment.objects.filter(rack=int(rack_id))
        # ป้องกัน error user ที่ไม่ได้ login
        try:
            context['user_data'] = UserData.objects.get(user=self.request.user.id)
        except:
            context['user'] = UserData.objects.last()
        return context

class deleteEquipment(DeleteView):
    model = Equipment
    success_url = reverse_lazy('equipment-listview')
    def get(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)

class EquipmentIPAddressDetail(DetailView):
    model = Equipment
    context_object_name = 'equipment'
    template_name = 'equipment/equipmentipaddress-report.html'
    # ส่งข้อมูลไปหน้า html 
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['ip_address'] = IPAddresss.objects.filter(equipment=self.kwargs['pk'])
        context['customer'] = Customer.objects.all()
        # ป้องกัน error user ที่ไม่ได้ login
        try:
            context['user_data'] = UserData.objects.get(user=self.request.user.id)
        except:
            context['user'] = UserData.objects.last()
        return context