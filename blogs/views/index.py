from django.shortcuts import render, redirect
from django.views.generic import CreateView, UpdateView, ListView, TemplateView
from blogs.models.ip import IPNetworkk
from blogs.models.equipment import Equipment, RackEquipment
from blogs.models.service import Service
from blogs.models.rack import Rack
from blogs.models.user_data import UserData
from django.contrib.auth.models import User
from blogs.models.location import Location

class IndexView(TemplateView):
    template_name = 'index.html'
    # ส่งข้อมูลไปหน้า html 
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        try:
            if not UserData.objects.filter(user=self.request.user.id):
                UserData(user=User.objects.get(id=self.request.user.id)).save()
        except:
            print("pleass login")
        context['ipnetwork'] = IPNetworkk.objects.all()
        context['equipment'] = Equipment.objects.all()
        context['rackequipment'] = RackEquipment.objects.all()
        context['service'] = Service.objects.all()
        context['rack'] = Rack.objects.all()
        context['location'] = Location.objects.all()
        try:
            context['user_data'] = UserData.objects.get(user=self.request.user.id)
        except:
            context['user'] = UserData.objects.last()
        return context

class LinkLoginRegisterView(TemplateView):
    template_name = 'link-login-register.html'