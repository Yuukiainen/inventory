from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from blogs.models.user_data import UserData

class RegistrationForm(UserCreationForm):

    user_status = forms.CharField()
    class Meta:
        model = User
        fields = (
                   'username',
                   'password1',
                   'password2',
                   'first_name',
                   'last_name',
                   'email',
                   'user_status',
                 )
    def clean_username(self, *args, **kwargs):
        username_input = self.cleaned_data.get('username')
        # ตวจสอบว่ามี user ที่ใช้ username นี้แล้วหรือยัง
        if User.objects.filter(username=username_input):
            self._errors['username'] = self.error_class(['**Username นี้เคยลงทะเบียนแล้ว']) 
        return username_input

class UpdateImageForm(forms.ModelForm):

    # รับข้อมูลจากหน้า View
    def __init__(self, *args, **kwargs):
        self.user_data_id = kwargs.pop('user_data_id', None)
        super(UpdateImageForm, self).__init__(*args, **kwargs)

    class Meta:
        model = UserData
        fields = (
                    'image',
                 )
    def save(self):
        # save image ที่เป็นรูปโปรไฟล์
        user_data = UserData.objects.get(id=int(self.user_data_id))
        user_data.image = self.cleaned_data['image']
        user_data.save()


class UpdateUserDataForm(forms.ModelForm):

    class Meta:
        model = UserData
        fields = (
                    'first_name',
                    'last_name',
                    'email',
                    'phone',
                    'address',
                 )

