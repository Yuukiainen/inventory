from django import forms
from blogs.models.service import Service, ServiceEquipment, ServiceIPAddress, ServiceType
from blogs.models.ip import IPNetworkk, IPAddresss
from ipaddress import ip_network
from netaddr import IPNetwork, IPAddress
from blogs.models.equipment import Equipment
from django.db.models import Q

class AddServiceEquipmentForm(forms.ModelForm):

    class Meta:

        model = ServiceEquipment
        fields = (
                    'service',
                    'equipment',
                 )

class AddServiceIPAddressForm(forms.ModelForm):

    # รับข้อมูลจากหน้า View
    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        super(AddServiceIPAddressForm, self).__init__(*args, **kwargs)

    class Meta:
        model = ServiceIPAddress
        fields = (
                    'service',
                    'ip_network',
                    'ip_address',
                 )
    def clean(self, *args, **kwargs):
        service_input = self.cleaned_data.get('service')
        ip_address_input = self.cleaned_data.get('ip_address')
        # ตวจสอบว่า ip address เคยใช้ใน ServiceIPAddress นี้แล้วหรือยัง
        if ServiceIPAddress.objects.filter(ip_address=(IPAddresss.objects.get(ip_address=ip_address_input)).id):
            self._errors['service'] = self.error_class(['**IP Address นี้เคยลงทะเบียนแล้ว']) 
        # ตรวจสอบว่า equipment ที่เก็บ ip address นี้ไว้ ถูกเพิ่มไว้ใน service นี้แล้วหรือยัง ถ้ายังให้เพิ่ม
        if not ServiceEquipment.objects.filter(Q(service=service_input) and Q(equipment=(IPAddresss.objects.get(ip_address=ip_address_input)).equipment.id)):
            service_equipment_info = ServiceEquipment(
                                                        service = Service.objects.get(name=service_input),
                                                        equipment = Equipment.objects.get(id=(IPAddresss.objects.get(ip_address=ip_address_input)).equipment.id),
                                                    )
            service_equipment_info.save()
        return self.cleaned_data

class AddServiceForm(forms.ModelForm):

    class Meta:

        model = Service
        fields = (
                    'type_service',
                    'name',
                    'customer',
                    'description',
                 )
    def clean_name(self, *args, **kwargs):
        name = self.cleaned_data.get('name')
        # ตวจสอบว่ามี service ที่ใช้ชื่อนี้แล้วหรือยัง
        if Service.objects.filter(name=name):
            self._errors['type_service'] = self.error_class(['**Name Service นี้เคยลงทะเบียนแล้ว']) 
        return name

class AddServiceTypeForm(forms.ModelForm):
    class Meta:
        model = ServiceType
        fields = (
                    'type_service',
                 )
    def clean_type_service(self, *args, **kwargs):
        type_input = self.cleaned_data.get('type_service')
        # ตวจสอบว่ามี Type Service ที่ใช้ชื่อนี้แล้วหรือยัง
        if ServiceType.objects.filter(type_service=type_input):
            self._errors['type_service'] = self.error_class(['**Service ประเภทนี้เคยลงทะเบียนแล้ว']) 
        return type_input
            