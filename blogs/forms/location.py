from django import forms
from blogs.models.location import Location
from blogs.models.rack import Rack

class AddLocationForm(forms.ModelForm):
 
    address = forms.CharField(widget=forms.Textarea)

    class Meta:

        model = Location
        fields = (
                    'location',
                    'address',
                 )
    def clean_location(self, *args, **kwargs):
        location_input = self.cleaned_data.get('location')
        # ตรวจสอบว่า location นี้เคยถูกเพิ่มแล้วหรือยัง
        if(Location.objects.filter(location=location_input)):
            self._errors['location'] = self.error_class(['**Location นี้เคยถูกลงทะเบียนแล้ว'])
        return location_input
        
    def save(self, commit=True):
        instance = super(AddLocationForm, self).save(commit=False)
        # รับข้อมูลและเก็บข้อมูลเป็น Array
        address_input = (self.cleaned_data['address']).split('\r\n')
        address = ''
        # จัดรียง Address ใหม่ให้เรียงเป็นแถวเดียวกัน
        for x in address_input:
            address = address + x + ' '
        instance.location = self.cleaned_data['location']
        instance.address = address
        # save location
        if commit:
            instance.save()
        # save out of rack ไว้ใน location
        rack_info = Rack(
                            location = Location.objects.last(),
                            rack = 'Out of Rack',
                            acceptable_weight = 0,
                            rack_weight = 0,
                            rack_size = 0,
                        )
        rack_info.save()
        return instance

class UpdateLocationForm(forms.ModelForm):
 
    # รับข้อมูลจากหน้า View
    def __init__(self, *args, **kwargs):
        self.id = kwargs.pop('id', None)
        super(UpdateLocationForm, self).__init__(*args, **kwargs)

    address = forms.CharField(widget=forms.Textarea)

    class Meta:

        model = Location
        fields = (
                    'location',
                    'address',
                 )
    def clean_location(self, *args, **kwargs):
        location_input = self.cleaned_data.get('location')
        # ตวจสอบว่ามี location ที่ใช้ชื่อนี้แล้วหรือยังและไม่ใช่ location ของตัวมันเอง
        if(Location.objects.filter(location=location_input)):
            if (Location.objects.get(location=location_input)).id != self.id:
                self._errors['location'] = self.error_class(['**Location นี้เคยถูกลงทะเบียนแล้ว'])
        return location_input
