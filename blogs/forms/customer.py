from django import forms
from blogs.models.customer import Customer


class AddCustomerForm(forms.ModelForm):

    address = forms.CharField(widget=forms.Textarea)

    class Meta:

        model = Customer
        fields = (
                    'name',
                    'email',
                    'phone',
                    'address',
                 )

    def clean_name(self, *args, **kwargs):
        name_input = self.cleaned_data.get('name')
        # ตวจสอบว่ามี Customer ที่ใช้ชื่อนี้แล้วหรือยัง
        if(Customer.objects.filter(name=name_input)):
            self._errors['name'] = self.error_class(['**ท่านเคยลงทะเบียนแล้ว']) 
        return name_input

    def clean_address(self, *args, **kwargs):
        # รับข้อมูลและเก็บข้อมูลเป็น Array
        address_input = (self.cleaned_data.get('address')).split('\r\n')
        address = ''
        # จัดรียง Address ใหม่ให้เรียงเป็นแถวเดียวกัน
        for x in address_input:
            address = address + x + ' '
        return address

class UpdateCustomerForm(forms.ModelForm):

    address = forms.CharField(widget=forms.Textarea)

    class Meta:

        model = Customer
        fields = (
                    'name',
                    'email',
                    'phone',
                    'address',
                 )