from django import forms
from blogs.models.rack import Rack
from blogs.models.location import Location
from django.db.models import Q


class AddRackForm(forms.ModelForm):

    class Meta:

        model = Rack
        fields = (
                    'location',
                    'rack',
                    'acceptable_weight',
                    'rack_weight',
                    'rack_size',
                 )

    def clean(self, *args, **kwargs):
        location_input = self.cleaned_data.get('location')
        rack_input = self.cleaned_data.get('rack')
        validate = 0
        rack = Rack.objects.filter(rack=rack_input)
        # วนลูปตรวจสอบชื่อ rack ใน location เดียวกัน
        for x in rack:
            if str(x.location) == str(location_input):
                validate = 1
        if validate == 1:
            self._errors['location'] = self.error_class(['**Rack นี้เคยถูกลงทะเบียนแล้ว'])


class UpdateRackForm(forms.ModelForm):

 
    # รับข้อมูลจากหน้า View
    def __init__(self, *args, **kwargs):
        self.id = kwargs.pop('id', None)
        super(UpdateRackForm, self).__init__(*args, **kwargs)

    class Meta:

        model = Rack
        fields = (
                    'location',
                    'rack',
                    'acceptable_weight',
                    'rack_weight',
                    'rack_size',
                 )

    def clean(self, *args, **kwargs):
        location_input = self.cleaned_data.get('location')
        rack_input = self.cleaned_data.get('rack')
        validate = 0
        rack = Rack.objects.filter(rack=rack_input)
        rack_id = 0
        if rack:
            # วนลูปตรวจสอบชื่อ rack ใน location เดียวกัน
            for x in rack:
                if str(x.location) == str(location_input):
                    rack_id = x.id                
                    validate = 1
        if validate == 1:
            # ตวจสอบว่ามี rack ที่ใช้ชื่อนี้แล้วหรือยังและไม่ใช่ rack ของตัวมันเอง
            if (Rack.objects.get(id=rack_id)).id != self.id:
                self._errors['location'] = self.error_class(['**Rack นี้เคยถูกลงทะเบียนแล้ว'])