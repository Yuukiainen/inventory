from django import forms
from blogs.models.ip import IPNetworkk, IPAddresss
from ipaddress import IPv4Address, IPv6Address, ip_network
from netaddr import IPNetwork, IPAddress
from blogs.models.customer import Customer
from ipaddress import ip_network

class UpdateIPNetworkForm(forms.ModelForm):

    # แก้ไขฟอร์ม
    ip_network = forms.CharField()

    class Meta:

        model = IPNetworkk
        fields = (
                    'ip_network',
                    'subnet',
                    'v_lan',
                 )

class AddIPNetworkForm(forms.ModelForm):

    # แก้ไขฟอร์ม
    ip_network = forms.CharField()

    class Meta:

        model = IPNetworkk
        fields = (
                    'ip_network',
                    'subnet',
                    'v_lan',
                 )

    def clean(self, *args, **kwargs):
        ip_network_input = self.cleaned_data.get('ip_network')
        subnet_input = self.cleaned_data.get('subnet')
        Network = IPNetworkk.objects.all()

        validate = 0
        setNetwork = str(ip_network_input) + '/' + str(subnet_input)
        # ตรวจสอบ Error เมื่อเกิด error ถือว่าข้อมูลที่กรอกมาผิด
        try:
            # วนลูปตรวจสอบช่วง และ ip network นี้ซ้ำไหม
            for x in Network:
                getNetwork = str(x.ip_network) + '/' + str(x.subnet)
                
                address = ip_network(setNetwork)
                address2 = ip_network(getNetwork)
                if IPAddress(str(address[0])) in IPNetwork(getNetwork):
                    print('a')
                    validate = 1
                elif IPAddress(str(address[-1])) in IPNetwork(getNetwork):
                    print('b')
                    validate = 1

                elif IPAddress(str(address2[0])) in IPNetwork(setNetwork):
                    print('c')
                    validate = 1
                elif IPAddress(str(address2[-1])) in IPNetwork(setNetwork):
                    print('d')
                    validate = 1
            if validate == 1:
                self._errors['ip_network'] = self.error_class(['**IP Network นี้เคยลงทะเบียนเเล้ว']) 
            else:
                print('success')
        except:
            print('ERROR')
            self._errors['ip_network'] = self.error_class(['**IP Network หรือ Subnet ของท่านไม่ถูกต้อง']) 


class AddIPAddressForm(forms.ModelForm):
    class Meta:
        model = IPAddresss
        fields = (
                    'ip_network',
                    'ip_address',
                    'equipment',
                    'customer',
                    'description',
                 )

    def clean(self, *args, **kwargs):
        ip_network_input = self.cleaned_data.get('ip_network')
        ip_address_input = self.cleaned_data.get('ip_address')
        network = IPNetworkk.objects.get(ip_network=ip_network_input)
        ipnetwork = str(network.ip_network) + '/' + str(network.subnet)
        # ตรวจสอบว่า ip address นี้อยู่ในช่วงของ ip network ไหม
        if IPAddress(ip_address_input) in IPNetwork(ipnetwork):
            # ตรวจสอบว่า ip address นี้เคยใช้แล้วหรือยัง
            if IPAddresss.objects.filter(ip_address=str(ip_address_input)):
                self._errors['ip_network'] = self.error_class(['**IP Address นี้เคยลงทะเบียนเเล้ว']) 
        else:
            self._errors['ip_network'] = self.error_class(['**IP Network และ IP Address ของท่านไม่สอดคล้อง'])
            # ตรวจสอบว่า ip address นี้เป็น network หรือ broadcast ไหม
        if str(ip_address_input) == str(ip_network(ipnetwork)[0]) or str(ip_address_input) == str(ip_network(ipnetwork)[-1]):
            self._errors['ip_network'] = self.error_class(['**ไม่สามารถเพิ่ม Network หรือ Broadcast ได้'])

class DeleteIPAddressForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.ipnetwork = kwargs.pop('ipnetwork', None)
        super(DeleteIPAddressForm, self).__init__(*args, **kwargs)

    delete_ip = forms.CharField()
    class Meta:
        model = IPAddresss
        fields = (
                    'delete_ip',
                 )

    def save(self, *args, **kwargs):
        ip_address = super(DeleteIPAddressForm, self)
        ip_address_input = self.cleaned_data.get('delete_ip')
        # แยก ip address เป็น array ตามเครื่องหมาย ,
        id_ip_address = ip_address_input.split(',')
        # ตอมสอบว่าความยาวของ array ยาวเท่ากับจำนวน ip address ที่ถูกสร้างไว้ใน ip network นี้ไหม
        if str(len(IPAddresss.objects.filter(ip_network=self.ipnetwork.id))) == str(len(id_ip_address)):
            # ลบ ip address ที่ถูกสร้างไว้ใน ip network นี้ทั้งหมด
            (IPAddresss.objects.filter(ip_network=self.ipnetwork.id)).delete()
        else:
            # วนลูปลบ ip address ตามที่เลือกเอาไว้
            for x in id_ip_address:
                (IPAddresss.objects.get(id=int(x))).delete()
        return ip_address
