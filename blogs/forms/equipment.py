from django import forms
from blogs.models.equipment import Equipment, RackEquipment, MachineType
from django.contrib.auth.models import User
from blogs.models.ip import IPNetworkk, IPAddresss
from ipaddress import ip_network
from netaddr import IPNetwork, IPAddress
from blogs.models.rack import Rack
from blogs.models.location import Location
from django.contrib.staticfiles.storage import staticfiles_storage

class AddEquipmentForm(forms.ModelForm):

    # รับข้อมูลจากหน้า View
    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        super(AddEquipmentForm, self).__init__(*args, **kwargs)

    # แก้ไขฟอร์ม
    OPTIONS = (
                ("1", "unit_1"), ("2", "unit_2"), ("3", "unit_3"),
                ("4", "unit_4"), ("5", "unit_5"), ("6", "unit_6"),
                ("7", "unit_7"), ("8", "unit_8"), ("9", "unit_9"),
                ("10", "unit_10"), ("11", "unit_11"), ("12", "unit_12"),
                ("13", "unit_13"), ("14", "unit_14"), ("15", "unit_15"),
                ("16", "unit_16"), ("17", "unit_17"), ("18", "unit_18"),
                ("19", "unit_19"), ("20", "unit_20"), ("21", "unit_21"),
                ("22", "unit_22"), ("23", "unit_23"), ("24", "unit_24"),
                ("25", "unit_25"), ("26", "unit_26"), ("27", "unit_27"),
                ("28", "unit_28"), ("29", "unit_29"), ("30", "unit_30"),
                ("31", "unit_31"), ("32", "unit_32"), ("33", "unit_33"),
                ("34", "unit_34"), ("35", "unit_35"), ("36", "unit_36"),
                ("37", "unit_37"), ("38", "unit_38"), ("39", "unit_39"),
                ("40", "unit_40"), ("41", "unit_41"), ("42", "unit_42"),
             )

    location = forms.CharField(required=False)
    rack = forms.CharField(required=False)
    front_back = forms.CharField(required=False)
    unit_number = forms.MultipleChoiceField(widget=forms.CheckboxSelectMultiple,choices=OPTIONS,required=False)
    class Meta:

        model = Equipment
        fields = (
                    'serial_number',
                    'machine_name',
                    'machine_type',
                    'machine_weight',
                    'electric_power',
                    'customer',
                    'image',
                    'auto_generate',
                    'code_type',
                    'code',
                    'location',
                    'rack',
                    'front_back',
                    # 'unit_range',
                    'unit_number',
                 )
    
    
    def clean_serial_number(self, *args, **kwargs):
        serial_number = self.cleaned_data.get('serial_number')
        # ตวจสอบว่ามี Serial Number ที่ใช้ชื่อนี้แล้วหรือยัง
        if Equipment.objects.filter(serial_number=serial_number) and serial_number != "ฝาปิด":
            self._errors['serial_number'] = self.error_class(['**ซีเรียลนัมเบอร์นี้เคยลงทะเบียนแล้ว']) 
        return serial_number

    def clean(self, *args, **kwargs):
        unit_number_input = self.cleaned_data.get('unit_number')
        rack_input = self.cleaned_data.get('rack')
        
        # ตรวจสอบว่ามีการกรอก Unit Number ใหม่เมื่อไม่ใช่ Out of Rack
        if str((Rack.objects.get(id=int(rack_input))).rack) != 'Out of Rack':
            if len(unit_number_input) > 0:
                print('in rack')
                unit = unit_number_input[0]
                i = 1
                success = 1
                while i<len(unit_number_input):
                    if int(unit_number_input[i]) == int(unit) + i:
                        success = 1
                    else:
                        success = 0
                        break
                    i = i + 1
                if success == 0:
                    self._errors['serial_number'] = self.error_class(['**โปรดตรวจสอบ Unit Number']) 
            else:
                self._errors['serial_number'] = self.error_class(['**โปรดตรวจสอบ Unit Number/']) 
        else:
            print('out rack')

    def save(self, commit=True):
        # set value และ save equipment
        instance = super(AddEquipmentForm, self).save(commit=False)
        instance.serial_number = self.cleaned_data['serial_number']
        instance.machine_name = self.cleaned_data['machine_name']
        instance.machine_type = self.cleaned_data['machine_type']
        instance.machine_weight = self.cleaned_data['machine_weight']
        instance.electric_power = self.cleaned_data['electric_power']
        instance.customer = self.cleaned_data['customer']
        instance.image = self.cleaned_data['image']
        instance.auto_generate = self.cleaned_data['auto_generate']
        instance.code_type = self.cleaned_data['code_type']
        instance.code = self.cleaned_data['code']
        instance.user = self.user
        if commit:
            instance.save()
        
        rack = self.cleaned_data['rack']
        unit_number = self.cleaned_data['unit_number']
        front_back = self.cleaned_data['front_back']
        # save เมื่อไม่ใช่ Out of Rack
        if str((Rack.objects.get(id=int(rack)).rack)) != 'Out of Rack':
            if len(unit_number) > 0:
                setUnit = unit_number
                # วนลูป save unit ที่ละ unit
                for getUnit in setUnit:
                    rackequipment_info = RackEquipment(
                                                        equipment = Equipment.objects.last(),
                                                        front_back = front_back,
                                                        rack = Rack.objects.get(id=int(rack)),
                                                        unit_number = getUnit,
                                                    )
                    rackequipment_info.save()
        # save เมื่อใช่ Out of Rack
        else:
            rackequipment_info = RackEquipment(
                                                equipment = Equipment.objects.last(),
                                                front_back = '-',
                                                rack = Rack.objects.get(id=int(rack)),
                                                unit_number = '-',
                                              )
            rackequipment_info.save()
        return instance


class UpdateEquipmentForm(forms.ModelForm):

    class Meta:

        model = Equipment
        fields = (
                    'serial_number',
                    'machine_name',
                    'machine_type',
                    'machine_weight',
                    'electric_power',
                    'customer',
                    'image',
                    'auto_generate',
                    'code_type',
                    'code',
                 )

    # รับข้อมูลจากหน้า View
    def __init__(self, *args, **kwargs):
        self.id = kwargs.pop('id', None)
        super(UpdateEquipmentForm, self).__init__(*args, **kwargs)

    def clean(self, *args, **kwargs):
        serial_number = self.cleaned_data.get('serial_number')
        # ตวจสอบว่ามี Serial Number ที่ใช้ชื่อนี้แล้วหรือยังและไม่ใช่ Serial Number ของตัวมันเอง
        if Equipment.objects.filter(serial_number=serial_number):
            if (Equipment.objects.get(serial_number=serial_number)).id != self.id:
                self._errors['serial_number'] = self.error_class(['**ซีเรียลนัมเบอร์นี้เคยลงทะเบียนแล้ว']) 
        return self.cleaned_data
    

class AddEquipmentTypeForm(forms.ModelForm):
    class Meta:
        model = MachineType
        fields = (
                    'type_machine',
                 )
    def clean_type_machine(self, *args, **kwargs):
        type_input = self.cleaned_data.get('type_machine')
        # ตวจสอบว่ามี Type Equipment ที่ใช้ชื่อนี้แล้วหรือยัง
        if MachineType.objects.filter(type_machine=type_input):
            self._errors['type_machine'] = self.error_class(['**เครื่องประเภทนี้เคยลงทะเบียนแล้ว']) 
        return type_input
            
class MoveRachForm(forms.ModelForm):

    # แก้ไขฟอร์ม
    OPTIONS = (
                ("1", "unit_1"), ("2", "unit_2"), ("3", "unit_3"),
                ("4", "unit_4"), ("5", "unit_5"), ("6", "unit_6"),
                ("7", "unit_7"), ("8", "unit_8"), ("9", "unit_9"),
                ("10", "unit_10"), ("11", "unit_11"), ("12", "unit_12"),
                ("13", "unit_13"), ("14", "unit_14"), ("15", "unit_15"),
                ("16", "unit_16"), ("17", "unit_17"), ("18", "unit_18"),
                ("19", "unit_19"), ("20", "unit_20"), ("21", "unit_21"),
                ("22", "unit_22"), ("23", "unit_23"), ("24", "unit_24"),
                ("25", "unit_25"), ("26", "unit_26"), ("27", "unit_27"),
                ("28", "unit_28"), ("29", "unit_29"), ("30", "unit_30"),
                ("31", "unit_31"), ("32", "unit_32"), ("33", "unit_33"),
                ("34", "unit_34"), ("35", "unit_35"), ("36", "unit_36"),
                ("37", "unit_37"), ("38", "unit_38"), ("39", "unit_39"),
                ("40", "unit_40"), ("41", "unit_41"), ("42", "unit_42"),
             )

    
    location = forms.CharField(required=False)
    front_back = forms.CharField(required=False)
    unit_number = forms.MultipleChoiceField(widget=forms.CheckboxSelectMultiple,choices=OPTIONS,required=False)

    class Meta:
        model = RackEquipment
        fields = (
                    'equipment',
                    'location',
                    'rack',
                    'front_back',
                    # 'unit_range',
                    'unit_number',
                 )
    def clean(self, *args, **kwargs):
        unit_number_input = self.cleaned_data.get('unit_number')
        rack_input = self.cleaned_data.get('rack')
        # ตรวจสอบว่ามีการกรอก Unit Number ใหม่เมื่อไม่ใช่ Out of Rack
        if str(rack_input) != 'Out of Rack':
            if len(unit_number_input) > 0:
                print('in rack')
                unit = unit_number_input[0]
                i = 1
                success = 0
                while i<len(unit_number_input):
                    if int(unit_number_input[i]) == int(unit) + i:
                        success = 1
                    else:
                        success = 0
                        break
                    i = i + 1
                if success == 0:
                    self._errors['equipment'] = self.error_class(['**โปรดตรวจสอบ Unit Number']) 
            else:
                self._errors['equipment'] = self.error_class(['**โปรดตรวจสอบ Unit Number']) 
        else:
            print('out rack')

    def save(self, commit=True):
        instance = super(MoveRachForm, self)
        rack_input = self.cleaned_data.get('rack')
        unit_number_input = self.cleaned_data.get('unit_number')
        equipment = self.cleaned_data['equipment']
        rack = self.cleaned_data['rack']
        unit_number = self.cleaned_data['unit_number']
        front_back = self.cleaned_data['front_back']
        # ลบ unit ที่เคยใช้
        (RackEquipment.objects.filter(equipment=(Equipment.objects.get(serial_number=str(equipment)).id))).delete()
        
        # save เมื่อไม่ใช่ Out of Rack
        if str(rack_input) != 'Out of Rack':
            if len(unit_number_input) > 0:
                setUnit = unit_number
                # วนลูป save unit ที่ละ unit
                for getUnit in setUnit:
                    rackequipment_info = RackEquipment(
                                                        equipment = equipment,
                                                        front_back = front_back,
                                                        rack = rack,
                                                        unit_number = getUnit,
                                                    )
                    rackequipment_info.save()
        # save เมื่อใช่ Out of Rack
        else:
            rackequipment_info = RackEquipment(
                                                equipment = equipment,
                                                front_back = '-',
                                                rack = rack,
                                                unit_number = '-',
                                              )
            rackequipment_info.save()
        return instance