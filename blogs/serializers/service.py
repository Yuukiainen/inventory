from rest_framework import serializers
from blogs.models.service import ServiceType, Service, ServiceEquipment, ServiceIPAddress
from django.core.exceptions import ValidationError
from ipaddress import ip_network
from netaddr import IPNetwork, IPAddress
from blogs.models.ip import IPNetworkk, IPAddresss


class ServiceTypeSerializers(serializers.ModelSerializer):

    class Meta:
        model = ServiceType
        fields = (
                    'id',
                    'type_service',
                 )

    def validate(self, data):
        type_service_input = data.get('type_service', None)
        if ServiceType.objects.filter(type_service=type_service_input):
            raise ValidationError("ประเภทนี้เคยลงทะเบียนแล้ว")
        else:
            return data

class ServiceSerializers(serializers.ModelSerializer):

    class Meta:
        model = Service
        fields = (
                    'id',
                    'type_service',
                    'url',
                    'purpose',
                    'user',
                    'customer',
                    'note',
                 )

    def validate(self, data):
        url = data.get('url', None)
        if Service.objects.filter(url=url):
            raise ValidationError("URL นี้เคยลงทะเบียนแล้ว")
        else:
            return data
            
class ServiceEquipmentSerializers(serializers.ModelSerializer):

    class Meta:
        model = ServiceEquipment
        fields = (
                    'id',
                    'service',
                    'equipment',
                 )
                 
class ServiceIPAddressSerializers(serializers.ModelSerializer):

    class Meta:
        model = ServiceIPAddress
        fields = (
                    'id',
                    'service',
                    'ip_network',
                    'ip_address',
                 )
    def validate(self, data):
        ip_network_input = data.get('ip_network')
        ip_address_input = data.get('ip_address')
        print(ip_address_input)
        network = IPNetworkk.objects.get(ip_network=ip_network_input)
        ipnetwork = str(network.ip_network) + '/' + str(network.subnet)
        if IPAddress(str(ip_address_input)) in IPNetwork(ipnetwork):
            if IPAddresss.objects.filter(ip_address=str(ip_address_input)):
                raise ValidationError("IP Address นี้เคยลงทะเบียนเเล้ว")
            else:
                return data
        else:
            raise ValidationError("IP Network และ IP Address ของท่านไม่สอดคล้อง")