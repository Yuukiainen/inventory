from rest_framework import serializers
from blogs.models.customer import Customer
from blogs.models.ip import IPNetworkk, IPAddresss
from django.core.exceptions import ValidationError
from netaddr import IPNetwork, IPAddress
from ipaddress import ip_network

class IPNetworkSerializers(serializers.ModelSerializer):
    
    class Meta:
        model = IPNetworkk
        fields = (
                    'id',
                    'ip_network',
                    'subnet',
                    'user',
                    'note',
                 )
    def validate(self, data):
        ip_network_input = data.get('ip_network', None)
        subnet_input = data.get('subnet', None)
        Network = IPNetworkk.objects.all()
        validate = 0
        setNetwork = str(ip_network_input) + '/' + str(subnet_input)
        try:
            for x in Network:
                getNetwork = str(x.ip_network) + '/' + str(x.subnet)
                
                address = ip_network(setNetwork)
                address2 = ip_network(getNetwork)
                if IPAddress(str(address[0])) in IPNetwork(getNetwork):
                    print('a')
                    validate = 1
                elif IPAddress(str(address[-1])) in IPNetwork(getNetwork):
                    print('b')
                    validate = 1

                elif IPAddress(str(address2[0])) in IPNetwork(setNetwork):
                    print('c')
                    validate = 1
                elif IPAddress(str(address2[-1])) in IPNetwork(setNetwork):
                    print('d')
                    validate = 1
            if validate == 1:
                raise ValidationError("IP Network นี้เคยลงทะเบียนเเล้ว")
            else:
                return data
        except:
            raise ValidationError("IP Network หรือ Subnet ของท่านไม่ถูกต้อง")

class IPAddressSerializers(serializers.ModelSerializer):

    class Meta:
        model = IPAddresss
        fields = (
                    'id',
                    'ip_network',
                    'ip_address',
                    'customer',
                    'discription',
                 )
    def validate(self, data):
        ip_network_input = data.get('ip_network')
        ip_address_input = data.get('ip_address')
        network = IPNetworkk.objects.get(ip_network=ip_network_input)
        ipnetwork = str(network.ip_network) + '/' + str(network.subnet)
        print(ipnetwork)
        if IPAddress(ip_address_input) in IPNetwork(ipnetwork):
            if IPAddresss.objects.filter(ip_address=str(ip_address_input)):
                raise ValidationError("IP Address นี้เคยลงทะเบียนเเล้ว")
            else:
                return data
        else:
            raise ValidationError("IP Network และ IP Address ของท่านไม่สอดคล้อง")