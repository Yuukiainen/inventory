from rest_framework import serializers
from blogs.models.equipment import Equipment, RackEquipment, MachineType
from blogs.models.rack import Rack
from django.core.exceptions import ValidationError



class EquipmentSerializers(serializers.ModelSerializer):
    code_type = serializers.ChoiceField(choices=(('BarCode','BarCode'),('QR Code','QR Code')))
    class Meta:
        model = Equipment
        fields = (
                    'id',
                    'serial_number',
                    'machine_name',
                    'machine_type',
                    'machine_weight',
                    'electric_power',
                    'user',
                    'customer',
                    'image',
                    'code_type',
                    'code',
                    'auto_generate',
                 )
    def validate(self, data):
        serial_number_input = data.get("serial_number", None)
        if Equipment.objects.filter(serial_number=serial_number_input) and serial_number_input != "ฝาปิด":
            raise ValidationError("ซีเรียลนัมเบอร์นี้เคยลงทะเบียนแล้ว")
        else:
            return data

class RackEquipmentSerializers(serializers.ModelSerializer):
    class Meta:
        model = RackEquipment
        fields = (
                    'id',
                    'equipment',
                    'rack',
                    'front_back',
                    'unit_number',
                 )
    def validate(self, data):
        equipment_input = data.get("equipment", None)
        rack_input = data.get("rack", None)
        unit_number_input = data.get("unit_number", None)
        rack = Rack.objects.get(rack=rack_input)
        weight = float(rack.acceptable_weight)
        weight = float(weight) - float(rack.rack_weight)
        weight = float(weight) - float(rack.legs_weight)
        rack_equipment = RackEquipment.objects.filter(unit_number=unit_number_input)
        serial_number = ''
        try:
            print(int(unit_number_input))
        except:
            raise ValidationError("ยูนิตไม่ถูกต้อง ยูนิตต้องมเป็นนตัวเลขจำนวนเต็มและมีค่าตั้งแต่ 1 -42 เท่านั้น")
        for x in rack_equipment:
            if serial_number != str(x.equipment):
                serial_number = str(x.equipment)
                weight = float(weight) - float(x.equipment.machine_weight)
            if str(x.rack) == str(rack) and str(x.unit_number) == str(unit_number_input):
                raise ValidationError("ยูนิตซ้ำ")
        equipment = Equipment.objects.get(serial_number=str(equipment_input))
        weight = float(weight) - float(equipment.machine_weight)
        if weight < 0:
            raise ValidationError("น้ำหนักเกิน")
        else:
            if int(unit_number_input) > 0 and int(unit_number_input) < 43:
                return data
            else:
                raise ValidationError("ยูนิตไม่ถูกต้อง ยูนิตต้องมเป็นนตัวเลขจำนวนเต็มและมีค่าตั้งแต่ 1 -42 เท่านั้น")
        # raise ValidationError("ซีเรียลนัมเบอร์นี้เคยลงทะเบียนแล้ว")


class EquipmentTypeSerializers(serializers.ModelSerializer):

    class Meta:
        model = MachineType
        fields = (
                    'id',
                    'type_machine',
                 )
    def validate(self, data):
        type_machine_input = data.get("type_machine", None)

        if MachineType.objects.filter(type_machine=type_machine_input):
            raise ValidationError("ประเภทนี้เคยลงทะเบียนแล้ว")
        else:
            raise ValidationError("ประเภทนี้เคยลงทะเบียนแล้ว")
            # return data

class RackSerializers(serializers.ModelSerializer):
    
    class Meta:
        model = Rack
        fields = (
                    'id',
                    'rack',
                    'acceptable_weight',
                    'rack_weight',
                    'legs_weight',
                 )
    def validate(self, data):
        rack_input = data.get("rack", None)
        if Rack.objects.filter(rack=rack_input):
            raise ValidationError("ตู้นี้เคยลงทะเบียนแล้ว")
        else:
            return data