from rest_framework import serializers
from blogs.models.customer import Customer
from django.core.exceptions import ValidationError

class CustomerSerializers(serializers.ModelSerializer):
    
    class Meta:
        model = Customer
        fields = (
                    'id',
                    'name',
                    'email',
                    'phone',
                    'address',
                 )
    def validate(self, data):
        name_input = data.get("name", None)
        if Customer.objects.filter(name=name_input):
            raise ValidationError("ชื่อนี้เคยลงทะเบียนแล้ว")
        else:
            return data