# Generated by Django 2.2.2 on 2019-08-05 08:50

import datetime
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('blogs', '0016_auto_20190803_2118'),
    ]

    operations = [
        migrations.CreateModel(
            name='IPAddresss',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('ip_address', models.GenericIPAddressField()),
                ('discription', models.CharField(default='', max_length=140)),
                ('create_at', models.DateField(default=datetime.date.today)),
                ('update_at', models.DateField(default=datetime.date.today)),
            ],
        ),
        migrations.CreateModel(
            name='IPNetworkk',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('ip_network', models.GenericIPAddressField()),
                ('subnet', models.IntegerField()),
                ('create_at', models.DateField(default=datetime.date.today)),
                ('note', models.TextField(blank=True, default='', null=True)),
                ('customer', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='blogs.Customer')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.RemoveField(
            model_name='setipnetwork',
            name='customer',
        ),
        migrations.RemoveField(
            model_name='setipnetwork',
            name='user',
        ),
        migrations.DeleteModel(
            name='setIPAddress',
        ),
        migrations.DeleteModel(
            name='setIPNetwork',
        ),
        migrations.AddField(
            model_name='ipaddresss',
            name='ip_network',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='blogs.IPNetworkk'),
        ),
    ]
