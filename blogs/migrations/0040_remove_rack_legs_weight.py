# Generated by Django 2.2.2 on 2019-09-10 16:38

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blogs', '0039_auto_20190910_1344'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='rack',
            name='legs_weight',
        ),
    ]
