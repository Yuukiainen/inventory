# Generated by Django 2.2.2 on 2019-09-17 01:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blogs', '0044_auto_20190916_1237'),
    ]

    operations = [
        migrations.AddField(
            model_name='ipnetworkk',
            name='v_lan',
            field=models.IntegerField(default=0),
            preserve_default=False,
        ),
    ]
