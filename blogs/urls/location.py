from django.urls import path
from blogs.views.location import  LocationView, AddLocationTamplateView, deleteLocation, UpdateLocation

urlpatterns = [
    path('<int:pk>/update-location/', UpdateLocation.as_view(), name='update-location'),
    path('add-location/', AddLocationTamplateView.as_view(), name='add-location-tamplate-view'),
    path('listview/', LocationView.as_view(), name='location-listview'),
    path('<int:pk>/delete/', deleteLocation.as_view(), name='location-delete'),
]