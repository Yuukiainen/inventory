from django.urls import path, include
from rest_framework import routers
from blogs.views.api import EquipmentSerializersView, RackEquipmentSerializersView, MachineTypeSerializersView, RackSerializersView, IPNetworkSerializersView, IPAddressSerializersView, CustomerSerializersView, ServiceTypeSerializersView, ServiceSerializersView, ServiceEquipmentSerializersView, ServiceIPAddressSerializersView

router = routers.DefaultRouter()
router.register('equipments', EquipmentSerializersView, 'equipments')
router.register('rackequipments', RackEquipmentSerializersView, 'rackequipments')
router.register('eqipment_types', MachineTypeSerializersView, 'eqipment_types')
router.register('racks', RackSerializersView, 'racks')

router.register('ipnetworks', IPNetworkSerializersView, 'ipnetworks')
router.register('ipaddress', IPAddressSerializersView, 'ipaddress')

router.register('service_types', ServiceTypeSerializersView, 'service_types')
router.register('service', ServiceSerializersView, 'service')
router.register('service_ip_networks', ServiceEquipmentSerializersView, 'service_ip_networks')
router.register('service_ip_address', ServiceIPAddressSerializersView, 'service_ip_address')

router.register('customers', CustomerSerializersView, 'customers')

urlpatterns = [
    path('', include(router.urls), name='api')
]
