from django.urls import path
from blogs.views.profile import ProfileView, UpdateProfile

urlpatterns = [
    path('<int:pk>/profile/', ProfileView.as_view(), name='profile'),
    path('<int:pk>/update-profile/', UpdateProfile.as_view(), name='update-profile'),
]