from django.urls import path
from blogs.views.service import ServiceView, AddService, DeleteService, AddServiceEquipment, AddServiceIPAddress, DeleteServiceIPAddress, DeleteServiceEquipment, ServiceDetail, AddServiceType

urlpatterns = [
    path('listview/', ServiceView.as_view(), name='service-listview'),
    path('add-service/', AddService.as_view(), name='add-service'),
    path('listview/add-serviceequipment/<int:pk>/', AddServiceEquipment.as_view(), name='add-serviceequipment'),
    path('listview/add-serviceipaddress/<int:pk>/', AddServiceIPAddress.as_view(), name='add-serviceipaddress'),
    path('delete-serviceipaddress/<int:pk>/', DeleteServiceIPAddress.as_view(), name='delete-service-ip-address'),
    path('delete-serviceequipment/<int:pk>/', DeleteServiceEquipment.as_view(), name='delete-service-equipment'),
    path('delete-service/<int:pk>/', DeleteService.as_view(), name='delete-service'),
    path('detail/<int:pk>/', ServiceDetail.as_view(), name='report-service'),
    path('add-service/add-service-type/', AddServiceType.as_view(), name='add-service-type'),
]