from django.urls import path
from blogs.views.rack import AddRack, RackView, DeleteRack, ReportRack, updateRack

urlpatterns = [
    path('location/<int:pk>/rack/listview/', RackView.as_view(), name='rack-listview'),
    path('<int:pk>/add/', AddRack.as_view(), name='add-rack'),
    path('<int:pk>/update/', updateRack.as_view(), name='update-rack'),
    path('delete/<int:pk>/', DeleteRack.as_view(), name='delete-rack'),
    path('report/<int:pk>/', ReportRack.as_view(), name='report-rack'),
]