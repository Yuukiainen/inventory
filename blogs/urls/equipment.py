from django.urls import path, include
from blogs.views.equipment import EquipmentView, AddEquipment, EquipmentDetail, updateEquipment, deleteEquipment, AddEquipmentType, MoveRack, EquipmentIPAddressDetail

urlpatterns = [
    path('listview/', EquipmentView.as_view(), name='equipment-listview'),
    path('add-equipment/', AddEquipment.as_view(), name='add-equipment'),
    path('detail/<int:pk>/', EquipmentDetail.as_view(), name='equipment-detail'),
    path('update/<int:pk>/', updateEquipment.as_view(), name='update-equipment'),
    path('delete/<int:pk>/', deleteEquipment.as_view(), name='delete-equipment'),
    path('add-equipment/add-equipment-type/', AddEquipmentType.as_view(), name='add-equipment-type'),
    path('<int:pk>/move-rack/', MoveRack.as_view(), name='move-rack'),
    path('<int:pk>/equipment-ip-address/', EquipmentIPAddressDetail.as_view(), name='equipment-ip-address'),
]
