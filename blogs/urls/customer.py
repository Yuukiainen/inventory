from django.urls import path
from blogs.views.customer import AddCustomer, CustomerView, CustomerDetail, AddCustomerTemplateView, UpdateCustomerTemplateView, deleteCustomer

urlpatterns = [
    path('listview/', CustomerView.as_view(), name='customer-listview'),
    path('<int:pk>/detail/', CustomerDetail.as_view(), name='customer-detail'),
    path('add-customer/', AddCustomer.as_view(), name='add-customer'),
    path('delete-customer/<int:pk>/', deleteCustomer.as_view(), name='delete-customer'),
    path('<int:pk>/update-customer/', UpdateCustomerTemplateView.as_view(), name='update-customer'),
    path('add-customer-template-view/', AddCustomerTemplateView.as_view(), name='add-customer-template-view'),
    path('equipment/add-equipment/add-customer/', AddCustomer.as_view(), name='add-customer'),
    path('ip/add-address/<int:pk>/add-customer/', AddCustomer.as_view(), name='add-customer'),
    path('service/add-service/add-customer/', AddCustomer.as_view(), name='add-customer'),
    path('equipment/update/<int:pk>/add-customer/', AddCustomer.as_view(), name='add-customer'),
]