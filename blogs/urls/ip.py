from django.urls import path
from blogs.views.ip import IPNetworkView, AddIPNetwork, AddIPAddress, IPNetworkDetail, DeleteIPNetwork, DeleteIPAddress, UpdateIPNetwork

urlpatterns = [
    path('listview/', IPNetworkView.as_view(), name='ip-listview'),
    path('add-network/', AddIPNetwork.as_view(), name='add-network'),
    path('<int:pk>/update-network/', UpdateIPNetwork.as_view(), name='update-network'),
    path('add-address/<int:pk>/', AddIPAddress.as_view(), name='add-address'),
    path('detail/<int:pk>/', IPNetworkDetail.as_view(), name='ip-detail'),
    path('delete/ip_address/<int:pk>/', DeleteIPAddress.as_view(), name='delete-address'),
    path('delete/ip_network/<int:pk>/', DeleteIPNetwork.as_view(), name='delete-network'),
]