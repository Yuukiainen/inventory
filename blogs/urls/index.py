from django.urls import path
from blogs.views.index import IndexView ,LinkLoginRegisterView

urlpatterns = [
    path('', IndexView.as_view(), name='index'),
    path('link-login-register/', LinkLoginRegisterView.as_view(), name='link-login-register'),
]