from django.db import models
from datetime import date
from blogs.models.customer import Customer
from django.contrib.auth.models import User
from django.urls import reverse
from blogs.models.equipment import Equipment

class IPNetworkk(models.Model):
    ip_network = models.CharField(max_length=140, default='')
    subnet = models.IntegerField()
    v_lan = models.CharField(max_length=140, default='')
    create_at = models.DateField(default=date.today)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.ip_network

class IPAddresss(models.Model):
    ip_address = models.CharField(max_length=140, default='')
    description = models.CharField(max_length=500, default='')
    ip_network = models.ForeignKey(IPNetworkk, on_delete=models.CASCADE) 
    equipment = models.ForeignKey(Equipment, on_delete=models.CASCADE) 
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE, blank=True, null=True) 
    create_at = models.DateField(default=date.today)
    update_at = models.DateField(default=date.today)

    def __str__(self):
        return self.ip_address