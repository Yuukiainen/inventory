from django.db import models
from blogs.models.customer import Customer
from django.contrib.auth.models import User
from blogs.models.rack import Rack
from django.dispatch import receiver
from django.db.models.signals import pre_save, pre_delete
import os


class UserData(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE) 
    first_name = models.CharField(max_length=1000, default='', blank=True, null=True)
    last_name = models.CharField(max_length=1000, default='', blank=True, null=True)
    email = models.CharField(max_length=1000, default='', blank=True, null=True)
    phone = models.CharField(max_length=1000, default='', blank=True, null=True)
    address = models.TextField(default='', blank=True, null=True)
    image = models.ImageField(blank=True, null=True)
    def __str__(self):
        return self.user.username


        
@receiver(pre_save, sender=UserData)
def delete_old_image(sender, instance, *args, **kwargs):
    if instance.pk:
        existing_image = UserData.objects.get(pk=instance.pk)
        if instance.image and existing_image.image != instance.image:
            existing_image.image.delete(True)

def _delete_file(path):
    if os.path.isfile(path):
        os.remove(path)

@receiver(pre_delete, sender=UserData)
def delete_image_pre_delete_product(sender, instance, *args, **kwargs):
    if instance.image:
        _delete_file(instance.image.path)