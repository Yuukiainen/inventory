from django.db import models

class Customer(models.Model):
    name = models.CharField(max_length=140, default='')
    email = models.EmailField()
    phone = models.CharField(max_length=140, default='')
    address = models.CharField(max_length=7000, default='')
    def __str__(self):
        return self.name