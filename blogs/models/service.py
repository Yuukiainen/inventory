from django.db import models
from blogs.models.customer import Customer
from django.contrib.auth.models import User
from blogs.models.equipment import Equipment
from datetime import date
from blogs.models.ip import IPNetworkk, IPAddresss


class ServiceType(models.Model):
    type_service = models.CharField(max_length=140, default='')
    def __str__(self):
        return self.type_service


class Service(models.Model):
    type_service = models.ForeignKey(ServiceType, on_delete=models.CASCADE) 
    name = models.CharField(max_length=140, default='')
    create_at = models.DateField(default=date.today)
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE, blank=True, null=True) 
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    description = models.CharField(max_length=140, default='', blank=True, null=True)
    def __str__(self):
        return self.name


class ServiceEquipment(models.Model):
    service = models.ForeignKey(Service, on_delete=models.CASCADE)
    equipment = models.ForeignKey(Equipment, on_delete=models.CASCADE)
    def __str__(self):
        return self.service.service

class ServiceIPAddress(models.Model):
    service = models.ForeignKey(Service, on_delete=models.CASCADE)
    ip_network = models.ForeignKey(IPNetworkk, on_delete=models.CASCADE)
    ip_address = models.ForeignKey(IPAddresss, on_delete=models.CASCADE)
    def __str__(self):
        return self.ip_address.ip_address
