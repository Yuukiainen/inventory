from django.db import models
from blogs.models.location import Location

class Rack(models.Model):
    location = models.ForeignKey(Location, on_delete=models.CASCADE)
    rack = models.CharField(max_length=140, default='')
    acceptable_weight = models.FloatField()
    rack_weight = models.FloatField()
    rack_size = models.IntegerField()
    def __str__(self):
        return self.rack