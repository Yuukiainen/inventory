from django.db import models
from blogs.models.customer import Customer
from django.contrib.auth.models import User
from blogs.models.rack import Rack
from django.dispatch import receiver
from django.db.models.signals import pre_save, pre_delete
import os


class MachineType(models.Model):
    type_machine = models.CharField(max_length=140, default='')
    def __str__(self):
        return self.type_machine
        
class Equipment(models.Model):
    serial_number = models.CharField(max_length=140, default='')
    machine_name = models.CharField(max_length=140, default='')
    machine_type = models.ForeignKey(MachineType, on_delete=models.CASCADE)
    machine_weight = models.FloatField(max_length=140, default='')
    electric_power = models.FloatField(max_length=140, default='')
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE, blank=True, null=True) 
    image = models.ImageField(default='___/___/')
    code_type = models.CharField(max_length=140, default='', blank=True, null=True)
    code = models.CharField(max_length=1000, default='', blank=True, null=True)
    auto_generate = models.BooleanField(default=True)
    def __str__(self):
        return self.serial_number

class RackEquipment(models.Model):
    equipment = models.ForeignKey(Equipment, on_delete=models.CASCADE)
    front_back = models.CharField(max_length=500, default='')
    rack = models.ForeignKey(Rack, on_delete=models.SET_NULL, blank=True, null=True)
    unit_number = models.CharField(max_length=500, default='')
    def __str__(self):
        return self.unit_number


        
@receiver(pre_save, sender=Equipment)
def delete_old_image(sender, instance, *args, **kwargs):
    if instance.pk:
        existing_image = Equipment.objects.get(pk=instance.pk)
        if instance.image and existing_image.image != instance.image:
            existing_image.image.delete(True)

def _delete_file(path):
    if os.path.isfile(path):
        os.remove(path)

@receiver(pre_delete, sender=Equipment)
def delete_image_pre_delete_product(sender, instance, *args, **kwargs):
    if instance.image:
        _delete_file(instance.image.path)