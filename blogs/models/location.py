from django.db import models

class Location(models.Model):
    location = models.CharField(max_length=5000, default='')
    address = models.CharField(max_length=5000, default='')
    def __str__(self):
        return self.location