FROM python:3-alpine
WORKDIR /usr/src/app
COPY requirements.txt ./
RUN apk add build-base zlib-dev jpeg-dev python3-dev
ENV LIBRARY_PATH=/lib:/usr/lib
RUN pip install --no-cache-dir -r requirements.txt
COPY . .
EXPOSE 8000
CMD [ "python", "manage.py", "runserver", "0.0.0.0:8000"]
