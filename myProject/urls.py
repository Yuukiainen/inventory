from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns


urlpatterns = [
    path('accounts/admin/', admin.site.urls, name='superuser'),    
    path('', include('blogs.urls.index')),
    path('ip/', include('blogs.urls.ip')),
    path('', include('blogs.urls.customer')),
    path('equipment/', include('blogs.urls.equipment')),
    path('service/', include('blogs.urls.service')),
    path('', include('blogs.urls.rack')),
    path('superuser/', include('blogs.urls.register')),
    path('accounts/', include('django.contrib.auth.urls')),
    path('api/', include('blogs.urls.api')),
    path('', include('blogs.urls.profile')),
    path('location/', include('blogs.urls.location')),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)